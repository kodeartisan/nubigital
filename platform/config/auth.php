<?php

return [

    'providers' => [
        'users' => [
            'model' => \Nubi\Platform\Domain\Users\Models\User::class,
        ],
    ],

];
