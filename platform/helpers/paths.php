<?php

if (!function_exists('platform_path')) {
    /**
     * Get the path to the fusion folder.
     *
     * @param string $path
     *
     * @return string
     */
    function platform_path(string $path = ''): string
    {
        return realpath(__DIR__.'/../').($path ? DIRECTORY_SEPARATOR.ltrim($path, DIRECTORY_SEPARATOR) : $path);
    }
}

if (! function_exists('plugin_path')) {
    /**
     * Get the path to the addons folder.
     *
     * @param string $path
     * @return string
     */
    function plugin_path(string $path = ''): string
    {
        return base_path().DIRECTORY_SEPARATOR.'plugins'.($path ? DIRECTORY_SEPARATOR.ltrim($path, DIRECTORY_SEPARATOR) : $path);
    }
}

if (! function_exists('core_path')) {
    /**
     * Get the path to the addons folder.
     *
     * @param string $path
     * @return string
     */
    function core_path(string $path = ''): string
    {
        return base_path().DIRECTORY_SEPARATOR.'core'.($path ? DIRECTORY_SEPARATOR.ltrim($path, DIRECTORY_SEPARATOR) : $path);
    }
}
