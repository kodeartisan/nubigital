<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sproutgigs_gigs', function (Blueprint $table) {
            $table->id();
            $table->uuid();
            $table->string('domain')->unique();
            $table->string('shortener')->unique();
            $table->json('posts');
            $table->json('proofs');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sproutgigs_gigs');
    }
};
