<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sproutgigs_gigs', function (Blueprint $table) {
           $table->string('mask')->nullable()->after('shortener');
           $table->text('notes')->nullable()->after('mask');
           $table->enum('type', ['DEFAULT', 'CODE'])->default('DEFAULT')->after('notes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sproutgigs_gigs', function (Blueprint $table) {
           $table->dropColumn('mask');
           $table->dropColumn('notes');
           $table->dropColumn('type');
        });
    }
};
