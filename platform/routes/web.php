<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;
use Nubi\Platform\Domain\SproutGigs\Models\BannedWord;
use Nubi\Platform\Domain\SproutGigs\Models\Gig;

Route::get('/bla', function () {
    return \Nubi\Platform\Domain\SproutGigs\Models\Gig::all(['shortener', 'proofs']);
});
Route::post('/webhook', function(Request $request) {
    Log::info($request->all());
});

Route::get('/update', function() {
    $bannedWords =  BannedWord::pluck('words');
    $shorteners = Gig::pluck('shortener');



    return $shorteners;
});
