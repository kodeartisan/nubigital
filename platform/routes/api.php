<?php

use Illuminate\Support\Facades\Route;
use Nubi\Platform\App\Api\Controllers\Auth\LoginController;
use Nubi\Platform\App\Api\Controllers\BannedWordsController;
use Nubi\Platform\App\Api\Controllers\SproutGigsController;

Route::post('/login', LoginController::class)->name('login');

Route::group(['middleware' => ['auth:sanctum']], function () {
    Route::get('sproutgigs/update', [SproutGigsController::class, 'updateData']);
    Route::get('sproutgigs/banned-words', [BannedWordsController::class, 'index']);
    Route::post('sproutgigs/gigs/search-by-domain', [SproutGigsController::class, 'searchByDomain']);
    Route::post('sproutgigs/gigs/search-by-shortener', [SproutGigsController::class, 'searchByShortener']);
});
