<?php

namespace Nubi\Platform\Providers;

use Filament\Facades\Filament;
use Filament\Navigation\NavigationGroup;
use Filament\PluginServiceProvider;
use Livewire\Livewire;
use Nubi\Platform\Domain\SproutGigs\Pages\BannedEmployers\CreateBannedEmployers;
use Nubi\Platform\Domain\SproutGigs\Pages\BannedEmployers\EditBannedEmployers;
use Nubi\Platform\Domain\SproutGigs\Pages\BannedEmployers\ListBannedEmployers;
use Nubi\Platform\Domain\SproutGigs\Pages\BannedWords\CreateBannedWords;
use Nubi\Platform\Domain\SproutGigs\Pages\BannedWords\EditBannedWords;
use Nubi\Platform\Domain\SproutGigs\Pages\BannedWords\ListBannedWords;
use Nubi\Platform\Domain\SproutGigs\Pages\Employers\CreateEmployers;
use Nubi\Platform\Domain\SproutGigs\Pages\Employers\EditEmployers;
use Nubi\Platform\Domain\SproutGigs\Pages\Employers\ListEmployers;
use Nubi\Platform\Domain\SproutGigs\Pages\Gigs\CreateGigs;
use Nubi\Platform\Domain\SproutGigs\Pages\Gigs\EditGigs;
use Nubi\Platform\Domain\SproutGigs\Pages\Gigs\ListGigs;
use Nubi\Platform\Domain\SproutGigs\Resources\BannedEmployersResource;
use Nubi\Platform\Domain\SproutGigs\Resources\BannedWordsResource;
use Nubi\Platform\Domain\SproutGigs\Resources\EmployersResource;
use Nubi\Platform\Domain\SproutGigs\Resources\GigsResource;


class FilamentServiceProvider extends  PluginServiceProvider
{
    public static string $name = 'platform';

    private array $components = [
        'nubi.platform.domain.sprout-gigs.pages.gigs.create-gigs' => CreateGigs::class,
        'nubi.platform.domain.sprout-gigs.pages.gigs.list-gigs' => ListGigs::class,
        'nubi.platform.domain.sprout-gigs.pages.gigs.edit-gigs' => EditGigs::class,
        'nubi.platform.domain.sprout-gigs.pages.banned-words.list-banned-words' => ListBannedWords::class,
        'nubi.platform.domain.sprout-gigs.pages.banned-words.edit-banned-words' => EditBannedWords::class,
        'nubi.platform.domain.sprout-gigs.pages.banned-words.create-banned-words' => CreateBannedWords::class,
        'nubi.platform.domain.sprout-gigs.pages.employers.create-employers' => CreateEmployers::class,
        'nubi.platform.domain.sprout-gigs.pages.employers.edit-employers' => EditEmployers::class,
        'nubi.platform.domain.sprout-gigs.pages.employers.list-employers' => ListEmployers::class,
        'nubi.platform.domain.sprout-gigs.pages.banned-employers.create-banned-employers' => CreateBannedEmployers::class,
        'nubi.platform.domain.sprout-gigs.pages.banned-employers.list-banned-employers' => ListBannedEmployers::class,
        'nubi.platform.domain.sprout-gigs.pages.banned-employers.edit-banned-employers' => EditBannedEmployers::class

    ];

    protected array $resources = [
        GigsResource::class,
        BannedWordsResource::class,
        EmployersResource::class,
        BannedEmployersResource::class
        //AuthorResource::class,

    ];

    public function boot()
    {
        $this->initComponents();
        $this->initNavigations();

    }

    private function initNavigations()
    {
        Filament::serving(function () {
            Filament::registerNavigationGroups([

                NavigationGroup::make()
                    ->label('SproutGigs')
                    ->icon('heroicon-s-cog')
                    ->collapsed(),
            ]);
        });
    }

    private function initComponents()
    {
        collect($this->components)
            ->each(function ($item, $key) {
                Livewire::component($key, $item);
            });
    }


}
