<?php

namespace Nubi\Platform\Providers;

use Exception;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Support\ServiceProvider;

class ScheduleServiceProvider extends ServiceProvider
{
    protected array $tasks = [

    ];

    /**
     * Schedule tasks.
     *
     * @return void
     * @throws Exception
     */
    public function boot(): void
    {
        $this->callAfterResolving(Schedule::class, function (Schedule $schedule) {
            collect($this->tasks)->each(function ($task) use ($schedule) {
                resolve($task)->handle($schedule);
            });
        });
    }
}
