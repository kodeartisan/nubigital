<?php

namespace Nubi\Platform\Providers;

use Illuminate\Support\ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected array $listen = [

    ];

    /**
     * The subscriber classes to register.
     *
     * @var array
     */
    protected array $subscribe = [

    ];
}
