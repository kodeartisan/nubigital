<?php

namespace Nubi\Platform\Providers;

use App\Http\Kernel;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;
use Laravel\Sanctum\Http\Middleware\EnsureFrontendRequestsAreStateful;

class PlatformServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        $this->registerServiceProviders();
        $this->registerMiddleware();
        $this->registerMacro();


    }

    public function boot(): void
    {
        $this->bootHelpers();
        $this->bootConfig();
        $this->bootMigrations();
        $this->bootRoutes();



    }

    protected function registerServiceProviders(): void
    {
        $this->app->register(EventServiceProvider::class);
        $this->app->register(ScheduleServiceProvider::class);
        $this->app->register(FilamentServiceProvider::class);
    }

    protected function registerMiddleware(): void
    {
        $kernel = $this->app->make(Kernel::class);

        $kernel->prependMiddlewareToGroup('api', EnsureFrontendRequestsAreStateful::class);
    }

    /**
     * Add `mergeDeep` macro to Arr Facade.
     *
     * @var array
     */
    protected function registerMacro()
    {
        Arr::macro('mergeDeep', function (array $arr1, array $arr2) {
            $output = array_merge($arr1, $arr2);

            foreach ($arr1 as $key => $value) {
                if (is_numeric($key) or !isset($arr2[$key])) {
                    continue;
                }

                if (is_array($value) && is_array($arr2[$key])) {
                    $output[$key] = Arr::mergeDeep($value, $arr2[$key]);
                    $output[$key] = array_unique($output[$key], SORT_REGULAR);
                }
            }

            return $output;
        });
    }

    protected function bootHelpers(): void
    {
        $files = File::glob(__DIR__ . '/../../helpers/*.php');
        foreach ($files as $file) {
            File::requireOnce($file);
        }
    }

    protected function bootConfig(): void
    {
        $files = File::files(platform_path('config'));

        foreach ($files as $file) {
            $path = $file->getPathname();
            $name = File::name($path);
            $this->app['config']->set(
                $name,
                Arr::mergeDeep(
                    $this->app['config']->get($name, []),
                    require $path,
                )
            );
        }
    }

    protected function bootMigrations(): void
    {
        $this->loadMigrationsFrom(platform_path('database/migrations'));
    }

    protected function bootRoutes(): void
    {
        $path = platform_path('routes');

        Route::group(['middleware' => ['api'], 'prefix' => 'api'], function () use ($path) {

            $this->loadRoutesFrom(realpath($path.'/api.php'));
        });

        Route::group(['middleware' => ['web']], function () use ($path) {
            $this->loadRoutesFrom(realpath($path.'/web.php'));
        });


    }
}
