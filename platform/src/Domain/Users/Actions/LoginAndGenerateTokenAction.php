<?php

namespace Nubi\Platform\Domain\Users\Actions;

use Nubi\Platform\App\Api\Requests\LoginRequest;
use Nubi\Platform\Domain\Users\Models\User;

class LoginAndGenerateTokenAction
{
    /**
     * @throws \Exception
     */
    public function execute(LoginRequest $request)
    {
        if (! auth()->attempt($request->only('email', 'password'))) {
            throw new \Exception('Email & Password does not match with our record.');
        }

        $user = User::whereEmail($request->email)->firstOrFail();
        $token = $user->createToken('authToken')->plainTextToken;

        return [
            'access_token' => $token,
            'token_type' => 'Bearer',
        ];


    }
}
