<?php

namespace Nubi\Platform\Domain\SproutGigs\Pages\Gigs;

use Filament\Pages\Actions\CreateAction;
use Filament\Resources\Pages\ListRecords;
use Nubi\Platform\Domain\SproutGigs\Resources\GigsResource;

class ListGigs extends ListRecords
{
    protected static string $resource = GigsResource::class;

    protected function getActions(): array
    {
        return [
            CreateAction::make(),
        ];
    }
}
