<?php

namespace Nubi\Platform\Domain\SproutGigs\Pages\Gigs;

use Filament\Pages\Actions\DeleteAction;
use Filament\Resources\Pages\EditRecord;
use Nubi\Platform\Domain\SproutGigs\Resources\GigsResource;

class EditGigs extends EditRecord
{
    protected static string $resource = GigsResource::class;

    protected function getActions(): array
    {
        return [
            DeleteAction::make()
        ];
    }
}
