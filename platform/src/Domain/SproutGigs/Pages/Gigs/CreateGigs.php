<?php

namespace Nubi\Platform\Domain\SproutGigs\Pages\Gigs;

use Filament\Resources\Pages\CreateRecord;
use Nubi\Platform\Domain\SproutGigs\Resources\GigsResource;

class CreateGigs extends CreateRecord
{
    protected static string $resource = GigsResource::class;
}
