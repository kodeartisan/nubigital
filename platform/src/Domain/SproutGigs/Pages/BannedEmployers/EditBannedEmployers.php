<?php

namespace Nubi\Platform\Domain\SproutGigs\Pages\BannedEmployers;

use Filament\Pages\Actions\DeleteAction;
use Filament\Resources\Pages\EditRecord;
use Nubi\Platform\Domain\SproutGigs\Resources\BannedEmployersResource;
use Nubi\Platform\Domain\SproutGigs\Resources\BannedWordsResource;

class EditBannedEmployers extends EditRecord
{
    protected static string $resource = BannedEmployersResource::class;

    protected function getActions(): array
    {
       return [
           DeleteAction::make()
       ];
    }
}
