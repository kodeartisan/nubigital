<?php

namespace Nubi\Platform\Domain\SproutGigs\Pages\BannedEmployers;

use Filament\Resources\Pages\CreateRecord;
use Nubi\Platform\Domain\SproutGigs\Resources\BannedEmployersResource;
use Nubi\Platform\Domain\SproutGigs\Resources\BannedWordsResource;

class CreateBannedEmployers extends CreateRecord
{
    protected static string $resource = BannedEmployersResource::class;
}
