<?php

namespace Nubi\Platform\Domain\SproutGigs\Pages\BannedEmployers;

use Filament\Pages\Actions\CreateAction;
use Filament\Resources\Pages\ListRecords;
use Nubi\Platform\Domain\SproutGigs\Resources\BannedEmployersResource;
use Nubi\Platform\Domain\SproutGigs\Resources\BannedWordsResource;

class ListBannedEmployers extends ListRecords
{
    protected static string $resource = BannedEmployersResource::class;

    protected function getActions(): array
    {
        return [
            CreateAction::make(),
        ];
    }
}
