<?php

namespace Nubi\Platform\Domain\SproutGigs\Pages\Employers;

use Filament\Resources\Pages\ListRecords;
use Nubi\Platform\Domain\SproutGigs\Resources\EmployersResource;

class ListEmployers extends ListRecords
{
    protected static string $resource = EmployersResource::class;
}
