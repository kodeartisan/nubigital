<?php

namespace Nubi\Platform\Domain\SproutGigs\Pages\Employers;

use Filament\Resources\Pages\CreateRecord;
use Nubi\Platform\Domain\SproutGigs\Resources\EmployersResource;

class CreateEmployers extends CreateRecord
{
    protected static string $resource = EmployersResource::class;
}
