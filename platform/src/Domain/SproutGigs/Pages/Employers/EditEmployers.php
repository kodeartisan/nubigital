<?php

namespace Nubi\Platform\Domain\SproutGigs\Pages\Employers;

use Filament\Resources\Pages\EditRecord;
use Nubi\Platform\Domain\SproutGigs\Resources\EmployersResource;

class EditEmployers extends EditRecord
{
    protected static string $resource = EmployersResource::class;
}
