<?php

namespace Nubi\Platform\Domain\SproutGigs\Pages\BannedWords;

use Filament\Pages\Actions\CreateAction;
use Filament\Resources\Pages\ListRecords;
use Nubi\Platform\Domain\SproutGigs\Resources\BannedWordsResource;

class ListBannedWords extends ListRecords
{
    protected static string $resource = BannedWordsResource::class;

    protected function getActions(): array
    {
        return [
            CreateAction::make(),
        ];
    }
}
