<?php

namespace Nubi\Platform\Domain\SproutGigs\Pages\BannedWords;

use Filament\Pages\Actions\DeleteAction;
use Filament\Resources\Pages\EditRecord;
use Nubi\Platform\Domain\SproutGigs\Resources\BannedWordsResource;

class EditBannedWords extends EditRecord
{
    protected static string $resource = BannedWordsResource::class;

    protected function getActions(): array
    {
       return [
           DeleteAction::make()
       ];
    }
}
