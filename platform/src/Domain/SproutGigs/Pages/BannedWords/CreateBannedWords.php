<?php

namespace Nubi\Platform\Domain\SproutGigs\Pages\BannedWords;

use Filament\Resources\Pages\CreateRecord;
use Nubi\Platform\Domain\SproutGigs\Resources\BannedWordsResource;

class CreateBannedWords extends CreateRecord
{
    protected static string $resource = BannedWordsResource::class;
}
