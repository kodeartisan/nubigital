<?php

namespace Nubi\Platform\Domain\SproutGigs\Resources;

use Filament\Forms\Components\Card;
use Filament\Forms\Components\Placeholder;
use Filament\Forms\Components\TextInput;
use Filament\Resources\Form;
use Filament\Resources\Resource;
use Filament\Resources\Table;
use Filament\Tables\Actions\DeleteAction;
use Filament\Tables\Actions\DeleteBulkAction;
use Filament\Tables\Actions\EditAction;
use Filament\Tables\Columns\TextColumn;
use Nubi\Platform\Domain\SproutGigs\Models\BannedEmployer;
use Nubi\Platform\Domain\SproutGigs\Models\BannedWord;
use Nubi\Platform\Domain\SproutGigs\Pages\BannedEmployers\CreateBannedEmployers;
use Nubi\Platform\Domain\SproutGigs\Pages\BannedEmployers\EditBannedEmployers;
use Nubi\Platform\Domain\SproutGigs\Pages\BannedEmployers\ListBannedEmployers;

class BannedEmployersResource extends Resource
{
    protected static ?string $model = BannedEmployer::class;

    protected static ?string $navigationIcon = 'heroicon-o-exclamation-circle';

    protected static ?string $navigationGroup = 'SproutGigs';

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Card::make()
                    ->schema([
                        TextInput::make('username')
                            ->unique(BannedEmployer::class, 'username', fn($record) => $record)
                    ])
                    ->columnSpan(3),
                Card::make()
                    ->schema([
                        Placeholder::make('created_at')
                            ->label('Last Modified')
                            ->content(fn(
                                ?BannedEmployer $record
                            ) : string => $record ? $record->updated_at->diffForHumans() : '-')
                    ])
                    ->columnSpan(1)
            ])
            ->columns(4);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                TextColumn::make('username')
                    ->sortable()
                    ->searchable()
            ])
            ->actions([
                EditAction::make(),
                DeleteAction::make()
            ])
            ->bulkActions([
                DeleteBulkAction::make()
            ]);
    }

    public static function getPages(): array
    {
        return [
            'index' => ListBannedEmployers::route('/'),
            'create' => CreateBannedEmployers::route('/create'),
            'edit' => EditBannedEmployers::route('/{record}/edit'),
        ];
    }


}
