<?php

namespace Nubi\Platform\Domain\SproutGigs\Resources;

use Filament\Forms\Components\Card;
use Filament\Forms\Components\Grid;
use Filament\Forms\Components\Placeholder;
use Filament\Forms\Components\Repeater;
use Filament\Forms\Components\Select;
use Filament\Forms\Components\Textarea;
use Filament\Forms\Components\TextInput;
use Filament\Forms\Components\Toggle;
use Filament\Resources\Form;
use Filament\Resources\Resource;
use Filament\Resources\Table;
use Filament\Tables\Actions\DeleteAction;
use Filament\Tables\Actions\DeleteBulkAction;
use Filament\Tables\Actions\EditAction;
use Filament\Tables\Columns\BadgeColumn;
use Filament\Tables\Columns\TextColumn;
use Filament\Tables\Filters\SelectFilter;
use Nubi\Platform\Domain\SproutGigs\Models\Gig;
use Nubi\Platform\Domain\SproutGigs\Pages\Gigs\CreateGigs;
use Nubi\Platform\Domain\SproutGigs\Pages\Gigs\EditGigs;
use Nubi\Platform\Domain\SproutGigs\Pages\Gigs\ListGigs;

class GigsResource extends Resource
{
    protected static ?string $model = Gig::class;

    protected static ?string $navigationIcon = 'heroicon-o-exclamation-circle';

    protected static ?string $navigationGroup = 'SproutGigs';

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
               Card::make()
                ->schema([
                        TextInput::make('domain')
                        ->required()
                        ->unique(Gig::class,'domain',  fn ($record) => $record)
                        ->url(),
                    TextInput::make('shortener')
                        ->required()
                        ->unique(Gig::class,'shortener',  fn ($record) => $record)
                        ->url(),
                    TextInput::make('mask')
                        ->url(),
                    Select::make('type')
                        ->options([
                           'DEFAULT' => 'Default',
                           'CODE' => 'Code',
                        ]),
                    Textarea::make('notes')
                        ->columnSpan(2),
                    Repeater::make('posts')
                        ->columnSpan(2)
                        ->required()
                        ->schema([
                            TextInput::make('url')
                                ->required()
                                ->url(),
                            TextInput::make('title'),
                            Textarea::make('last_paragraph')
                        ]),
                    Repeater::make('proofs')
                        ->columnSpan(2)
                        ->required()
                        ->schema([
                            Grid::make(2)
                                ->schema([
                                    TextInput::make('id')
                                        ->required(),
                                    Select::make('type')
                                        ->options([
                                            'default'            => 'Default',
                                            'url_1'              => '1st Url post visited',
                                            'url_2'              => '2nd Url post visited',
                                            'url_3'              => '3rd Url post visited',
                                            'url_4'              => '4nd Url post visited',
                                            'url_5'              => '5th Url post visited',
                                            'url_6'              => '6th Url post visited',
                                            'url_7'              => '7th Url post visited',
                                            'url_8'              => '8th Url post visited',
                                            'url_9'              => '9th Url post visited',
                                            'url_10'             => '10th Url post visited',
                                            'url_last_post_2'    => 'Last Url Post(2)',
                                            'url_last_post_3'    => 'Last Url Post(3)',
                                            'url_random_1'       => 'Random Post Url(1)',
                                            'url_random_2'       => 'Random Post Url(2)',
                                            'url_random_3'       => 'Random Post Url(3)',
                                            'title_2'              => '2nd Title post visited',
                                            'title_3'              => '3nd Title post visited',
                                            'title_4'              => '4nd Title post visited',
                                            'title_5'              => '5th Title post visited',
                                            'title_6'              => '6th Title post visited',
                                            'title_7'              => '7th Title post visited',
                                            'title_8'              => '8th Title post visited',
                                            'title_9'              => '9th Title post visited',
                                            'title_10'             => '10th Title post visited',
                                            'last_2_title'          => 'Last Title(2)',
                                            'last_3_title'          => 'Last Title(3)',
                                            'last_paragraph_5'   => 'Paragraph of the 5th',
                                            'last_paragraph_6'   => 'Paragraph of the 6th',
                                            'last_paragraph_7'   => 'Paragraph of the 7th',
                                            'last_paragraph_8'   => 'Paragraph of the 8th',
                                            'last_paragraph_9'   => 'Paragraph of the 9th',
                                            'last_paragraph_10'  => 'Paragraph of the 10th',
                                            'last_paragraph_posts_2' => 'Last paragraph(2)',
                                            'last_paragraph_posts_3' => 'Last paragraph(3)'

                                        ])
                                        ->default('default')

                                ]),
                            TextInput::make('description')
                                ->required(),
                            TextInput::make('proof')

                        ])


                ])
                ->columns([
                    'sm' => 2
                ])
                ->columnSpan(3),
                Card::make()
                ->schema([
                    Placeholder::make('created_at')
                        ->label('Last Modified')
                        ->content(fn(
                            ?Gig $record
                        ) : string => $record ? $record->updated_at->diffForHumans() : '-'),
                    Toggle::make('random_posts')->inline(false)->default(true)

                ])
                ->columnSpan(1)

            ])
            ->columns(4);


    }

    public static function table(Table $table): Table
    {
        return  $table
            ->columns([
                TextColumn::make('domain')
                    ->sortable()
                    ->searchable(),
                 TextColumn::make('shortener')
                     ->sortable()
                     ->searchable(),
                 BadgeColumn::make('type')
                     ->enum([
                         'DEFAULT' => 'Default',
                         'CODE' => 'Code'
                     ]),
                 TextColumn::make('created_at')
                    ->dateTime()
                    ->sortable()

            ])
            ->filters([
                SelectFilter::make('type')
                    ->options([
                        'DEFAULT' => 'Default',
                        'CODE' => 'Code'
                    ])
            ])
            ->actions([
                EditAction::make(),
                DeleteAction::make(),
            ])
            ->bulkActions([
                DeleteBulkAction::make()
            ])
            ->defaultSort('created_at', 'desc')
            ;
    }


    public static function getPages(): array
    {
        return [
            'index' => ListGigs::route('/'),
            'create' => CreateGigs::route('/create'),
            'edit' => EditGigs::route('/{record}/edit'),
        ];
    }
}
