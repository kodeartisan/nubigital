<?php

namespace Nubi\Platform\Domain\SproutGigs\Resources;

use Filament\Forms\Components\Card;
use Filament\Forms\Components\Placeholder;
use Filament\Forms\Components\TextInput;
use Filament\Resources\Form;
use Filament\Resources\Resource;
use Filament\Resources\Table;
use Filament\Tables\Actions\DeleteAction;
use Filament\Tables\Actions\DeleteBulkAction;
use Filament\Tables\Actions\EditAction;
use Filament\Tables\Columns\TextColumn;
use Nubi\Platform\Domain\SproutGigs\Models\BannedWord;
use Nubi\Platform\Domain\SproutGigs\Models\Employer;
use Nubi\Platform\Domain\SproutGigs\Pages\Employers\CreateEmployers;
use Nubi\Platform\Domain\SproutGigs\Pages\Employers\EditEmployers;
use Nubi\Platform\Domain\SproutGigs\Pages\Employers\ListEmployers;

class EmployersResource extends Resource
{
    protected static ?string $model = Employer::class;

    protected static ?string $navigationIcon = 'heroicon-o-exclamation-circle';

    protected static ?string $navigationGroup = 'SproutGigs';

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Card::make()
                    ->schema([
                        TextInput::make('username')
                            ->unique(Employer::class, 'username', fn($record) => $record)
                    ])
                    ->columnSpan(3),
                Card::make()
                    ->schema([
                        Placeholder::make('created_at')
                            ->label('Last Modified')
                            ->content(fn(
                                ?Employer $record
                            ) : string => $record ? $record->updated_at->diffForHumans() : '-')
                    ])
                    ->columnSpan(1)
            ])
            ->columns(4);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                TextColumn::make('username')
                    ->sortable()
                    ->searchable()
            ])
            ->actions([
                EditAction::make(),
                DeleteAction::make()
            ])
            ->bulkActions([
                DeleteBulkAction::make()
            ]);
    }

    public static function getPages(): array
    {
        return [
            'index' => ListEmployers::route('/'),
            'create' => CreateEmployers::route('/create'),
            'edit' => EditEmployers::route('/{record}/edit'),
        ];
    }


}
