<?php

namespace Nubi\Platform\Domain\SproutGigs\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Model;
use Nubi\Platform\Domain\Base\Traits\HasUuid;

class BannedWord extends Model
{
    use HasUuid;

    protected $table = 'sproutgigs_banned_words';

    protected $fillable = [
        'words',
    ];

    public function getRouteKeyName(): string
    {
        return 'uuid';
    }

}
