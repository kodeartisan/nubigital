<?php

namespace Nubi\Platform\Domain\SproutGigs\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Model;
use Nubi\Platform\Domain\Base\Traits\HasUuid;

class Gig extends Model
{
    use HasUuid;

    protected $table = 'sproutgigs_gigs';

    protected $fillable = [
        'domain',
        'shortener',
        'mask',
        'notes',
        'type',
        'posts',
        'proofs',
        'random_posts'
    ];

    public function getRouteKeyName(): string
    {
        return 'uuid';
    }

    protected function posts(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => json_decode($value, true),
            set: fn ($value) => json_encode($value)
        );
    }

    protected function proofs(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => json_decode($value, true),
            set: fn ($value) => json_encode($value)
        );
    }


}
