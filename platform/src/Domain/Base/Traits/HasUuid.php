<?php

namespace Nubi\Platform\Domain\Base\Traits;

use Illuminate\Database\Eloquent\Model;
use Ramsey\Uuid\Uuid;

trait HasUuid
{
    public static function bootHasUuid(): void
    {
        static::creating(function (Model $model) {
            $model->uuid = Uuid::uuid4()->toString();
        });

    }
}
