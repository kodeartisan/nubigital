<?php

namespace Nubi\Platform\App\Api\Controllers;

use Illuminate\Http\JsonResponse;
use Nubi\Platform\App\Base\BaseController;
use Nubi\Platform\Domain\SproutGigs\Models\BannedWord;

class BannedWordsController extends BaseController
{

    public function index(): JsonResponse
    {
        $bannedWords = BannedWord::all('words')->map(fn(BannedWord $word) => $word->words);

        return response()->json($bannedWords);

    }

}
