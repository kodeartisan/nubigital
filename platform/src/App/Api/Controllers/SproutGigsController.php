<?php

namespace Nubi\Platform\App\Api\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Support\Arr;
use Nubi\Platform\App\Api\Requests\SearchGigByDomainRequest;
use Nubi\Platform\App\Base\BaseController;
use Nubi\Platform\Domain\SproutGigs\Models\BannedEmployer;
use Nubi\Platform\Domain\SproutGigs\Models\BannedWord;
use Nubi\Platform\Domain\SproutGigs\Models\Employer;
use Nubi\Platform\Domain\SproutGigs\Models\Gig;

class SproutGigsController extends BaseController
{
    private array $proofTypes = [
        'url_1',
        'url_2',
        'url_3',
        'url_4',
        'url_5',
        'url_6',
        'url_7',
        'url_8',
        'url_9',
        'url_10',
        'url_random_1',
        'url_random_2',
        'url_last_post_2',
        'url_last_post_3',
        'last_paragraph_6',
        'last_paragraph_7',
        'last_paragraph_8',
        'last_paragraph_9',
        'last_paragraph_10'
    ];

    public function searchByDomain(SearchGigByDomainRequest $request): JsonResponse
    {
        try {
            $gig = Gig::whereDomain($request->domain)->firstOrFail();

            return response()->json($gig);
        } catch (\Exception $exception) {

            return  $this->respondError($exception);

        }

    }

    public function searchByShortener(SearchGigByDomainRequest $request): JsonResponse
    {
        try {
            $gig = Gig::whereShortener($request->domain)->firstOrFail();

            $items = $gig->random_posts ? Arr::shuffle($gig->posts) : $gig->posts;

            $proofs = collect($gig->proofs)->map(function ($proof, $key) use ($items) {

                $proof['proof'] =  match ($proof['type']) {
                  'url_1'               => $items[0]['url'],
                  'url_2'               => $items[1]['url'],
                  'url_3'               => $items[2]['url'],
                  'url_4'               => $items[3]['url'],
                  'url_5'               => $items[4]['url'],
                  'url_6'               => $items[5]['url'],
                  'url_7'               => $items[6]['url'],
                  'url_8'               => $items[7]['url'],
                  'url_9'               => $items[8]['url'],
                  'url_10'              => $items[9]['url'],
                  'title_2'               => $items[1]['title'],
                  'title_3'               => $items[2]['title'],
                  'title_4'               => $items[3]['title'],
                  'title_5'               => $items[4]['title'],
                  'title_6'               => $items[5]['title'],
                  'title_7'               => $items[6]['title'],
                  'title_8'               => $items[7]['title'],
                  'title_9'               => $items[8]['title'],
                  'title_10'              => $items[9]['title'],
                  'last_2_title'  => call_user_func(function() use ($items) {
                        $posts = array_slice($items, -2);

                        return $posts[0]['title']. "\n\n" .$posts[1]['title'];
                  }),
                'last_3_title'  => call_user_func(function() use ($items) {
                    $posts = array_slice($items, -3);

                    return $posts[0]['title']. "\n\n" .$posts[1]['title'] . "\n\n" .$posts[2]['title'];
                }),
                  'url_random_1'        => $items[array_rand($items)],
                  'url_random_2'        => call_user_func(function() use ($items) {

                      $urls = Arr::random($items, 2);

                      return $urls[0]['url']. "\n\n" .$urls[1]['url'];
                  }),
                  'url_random_3'        => call_user_func(function() use ($items) {

                      $urls = Arr::random($items, 3);

                      return $urls[0]['url']. "\n\n" .$urls[1]['url'] . "\n\n" .$urls[2]['url'];
                  }),
                  'url_last_post_2'  => call_user_func(function() use ($items) {
                      $urls = array_slice($items, -2);

                      return $urls[0]['url']. "\n\n" .$urls[1]['url'];
                  }),
                  'url_last_post_3' => call_user_func(function() use ($items) {
                      $urls = array_slice($items, -3);

                      return $urls[0]['url']. "\n\n" .$urls[1]['url'] . "\n\n" .$urls[2]['url'];
                  }),
                  'last_paragraph_5'    => $items[4]['last_paragraph'],
                  'last_paragraph_6'    => $items[5]['last_paragraph'],
                  'last_paragraph_7'    => $items[6]['last_paragraph'],
                  'last_paragraph_8'    => $items[7]['last_paragraph'],
                  'last_paragraph_9'    => $items[8]['last_paragraph'],
                  'last_paragraph_10'   => $items[9]['last_paragraph'],
                  'last_paragraph_posts_2'        => call_user_func(function() use ($items) {

                    $paragraphs = array_slice($items, -2);

                    return $paragraphs[0]['last_paragraph']. "\n\n" .$paragraphs[1]['last_paragraph'];
                 }),
                 'last_paragraph_posts_3'        => call_user_func(function() use ($items) {

                    $paragraphs = array_slice($items, -3);

                    return $paragraphs[0]['last_paragraph']. "\n\n" .$paragraphs[1]['last_paragraph']. "\n\n" .$paragraphs[2]['last_paragraph'];
                 }),
                  'default'             =>  $proof['proof']
                };

                return $proof;
            })->toArray();


            $gig->posts = $items;
            $gig->proofs = $proofs;

            return response()->json($gig);
        } catch (\Exception $exception) {

            return  $this->respondError($exception);

        }

    }

    public function updateData()
    {
        $bannedWords     =  BannedWord::pluck('words');
        $bannedEmployers = BannedEmployer::pluck('username');
        $shorteners      = Gig::pluck('shortener');
        $employers       = Employer::pluck('username');

        $data = [
            'banned_words'     => $bannedWords,
            'shorteners'       => $shorteners,
            'employers'        => $employers,
            'banned_employers' => $bannedEmployers
        ];

        return response()->json($data);
    }
}
