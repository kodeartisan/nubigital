<?php

namespace Nubi\Platform\App\Api\Controllers\Auth;

use Illuminate\Http\JsonResponse;
use Nubi\Platform\App\Api\Requests\LoginRequest;
use Nubi\Platform\App\Base\BaseController;
use Nubi\Platform\Domain\Users\Actions\LoginAndGenerateTokenAction;
use Nubi\Platform\Domain\Users\Traits\Auth\AuthenticatesUsers;
use Nubi\Platform\Domain\Users\Traits\Auth\ThrottlesLogins;
use Symfony\Component\HttpFoundation\Response;

class LoginController extends BaseController
{
    use ThrottlesLogins;
    use AuthenticatesUsers;

    public function __invoke(LoginRequest $request, LoginAndGenerateTokenAction $loginAndGenerateTokenAction): JsonResponse
    {
        try {
            $data = $loginAndGenerateTokenAction->execute($request);

            return response()->json($data, Response::HTTP_OK);

        } catch (\Exception $exception) {
            return $this->respondError($exception);
        }
    }
}
